+++
date = "2016-07-01T10:30:44-03:00"
menu = "main"
title = "Eixos"
type = "about"
weight = -210
+++

### 1. Automação

Devemos automatizar para termos controle, visão e produtividade.

- Automação
  - Gerência de Configurações
      - Convergência
      - Controle de estados
      - Idempotência
      - Versionamento
      - Testes
         - Syntax
         - Linter
         - Comportamento
         - Aceitação
  - Provisionamento
      - Autoserviço
         - Bare Metal
         - VMs
         - Containers
  - Orquestração
      - Push
      - MQ

Não existem mais sistemas simples, todos os sistemas são complexos e interdependentes. Hoje em dia manter um simples blog significa manter serviços como servidor de aplicação, banco de dados, cache, autenticação, estatísticas, dns, firewall, backup dentre outros componentes. Administrar tudo isso manualmente se torna inviável pois um cliente pode rodar centenas ou milhares de sites, serviços e sistemas com características similares a estas. Dentro deste contexto, podemos dizer que a automação é um dos principais pilares da Infra Ágil pois não há mais cenários que permitem administrar sistemas e serviços no modelo artesão.

Automação se torna obrigatória principalmente em ambientes que utilizam virtualização, nuvem ou containers.

Não podemos mais levar dias, semanas ou meses para atender demandas de nossos clientes, temos que conseguir atender
essas demandas em minutos através da automação.

#### 1.1 Automação: Gerência de Configurações

Atualmente quando falamos de automação em grande escala, o principal modelo que encontramos nesses projetos é a **Gerência de Configurações** (Configuration Management - **CM**).

O conceito de CM para sistemas computacionais foi criado pelo professor Mark Burgess durante o período em que fazia um pós-doutorado na universidade de Oslo na Noruega em 1993. Nesta época ele ajudava na administração do laboratório UNIX do departamento de física teórica. Durante esse período ele enfrentou alguns problemas com o ambiente do laboratório, para resolver tais problemas ele escreveu a primeira versão da ferramenta CFEngine.

O CFEngine era um projeto que reunia conceitos de Configurations Management encontrados na engenharia de software com os princípios de modelagem científica da ciência de computação, aplicando isto para a gestão de sistemas computacionais. Essa abordagem permitiu a criação da noção de operações convergentes.

Sistemas de automação com operações convergentes conseguem convergir para um estado final definido
pelo usuário, com isso, caso a ferramenta encontrasse
alguma característica que divergia do que o usuário
definiu, o sistema convergia para o estado definido.

Com o CFEngine era possível definir estados para diferentes sistemas operacionais, escondendo suas diferenças através de uma DSL (linguagem de domínio específico). Essa linguagem declarativa permitia que o usuário trabalhasse com um alto nível de abstração que tinha foco no estado final que o sistema deveria ter.

Entre 1993 e 2003 o CFEngine e o modelo CM evoluíram e adquiriram outra características como self-healing, detecção de anomalias e comunicações seguras.

Em 2004 Mark Burgess organizou todo esse aprendizado com o CFEngine e CM na teoria Promisse que é um modelo distribuído de automação self-healing. Depois de 2004, muitas ferramentas surgiram influenciadas por essa teoria, dentre elas podemos destacar o Puppet e Chef que herdam muitas características do CFEngine.

Para um projeto de infra ágil, recomendamos que use ferramentas CM com os seguintes recursos:

- Arquitetura Master/Agent;
- Secure Communications;
- Camada de abstração;
- Sistema de convergência;
- Sistema de controle de estados;
- Self-healing;
- Enforcing;
- Idempotência;
- Linguagem declarativa;
- Sistema de relatórios;
- Sistema de processamento de relatórios;
- Visualização de eventos e mudanças;
- Report Processors;
- APIs.

Atualmente o PUPPET e CHEF oferecem todos esses recursos. Em seu projeto você deve avaliar quais dos requisitos são fundamentais para seu time ou cliente e escolher a ferramenta que melhor se encaixe.

**Como consigo identificar rapidamente se uma ferramenta é Gerência de Configuração?**

- Funcionam em arquitetura agente e servidor;
- O servidor armazena as configurações dos nodes;
- O node roda um agente autônomo;
- O agente busca configurações no servidor;
- O agente aplica as configurações no node;
- O agente executa a convergência e o self-healing;
- Usa-se uma linguagem declarativa para descrever os estados desejados;
- Idempotência é um princípio fundamental do agente;
- Há um robusto sistema de relatórios;
- Há um robusto sistema de registro de eventos e mudanças;
- Oferece alta visibilidade das mudanças;
- Oferece alta visibilidade do clico de vida do node;
- O servidor nunca se conecta nos nós;
- Apenas os nós se conectam ao servidor.
- Oferece recursos de integração por API.

**Sobre versionamento e testes**

Com a gerência de configurações a organização entra em uma parte do mundo da infraestrutura como código e passa a utilizar também ferramentas de controle de versão para gerenciar o código que representa sua infraestrutura.

Sua infraestrutura passa a ser desenvolvida como se fosse um software, isso possibilita o uso de diversos métodos e ferramentas para testes para código. Todo esse controle ajudará a garantir qualidade do código que será utilizado na organização.

Em infra ágil o uso de gerência de configurações é obrigatório para se ter um completo processo de automação.

#### 1.2 Automação: Orquestração

Se formos olhar para uma orquestra veremos que cada músico tem consigo o "código" que precisa executar de forma autônoma. Neste ambiente o maestro atua como um "sistema" de apoio para harmonizar e facilitar a integração, colaboração e o trabalho do grupo.

A orquestração em essência está muito mais próxima das ferramentas de CM, porém, no mundo da TI, este é um termo com diferentes concepções e entendimentos. Apesar da origem do termo vir de uma orquestra, na TI o termo ganhou outro ponto de vista.

No entendimento comum a orquestração é um mecanismo central que dispara ações em paralelo para sistemas operacionais de um datacenter.

Para nós, dentro do modelo infra ágil, vamos considerar que a orquestração consiste em executar algo de forma paralela ou não, em tempo real, em um sistema operacional ou em um grupo de sistemas operacionais.

Diferente dos sistemas de gerência de configuração, os orquestradores se preocupam em rodar o que você determinou nos nodes do seu
parque, mas não há uma preocupação com garantia de estado ou com a garantia de execução, idempotência, registro rigoroso de eventos ou self-healing. A abordagem das ferramentas é mais objetiva e simples, o que por um lado é bom pois oferece uma pequena curva de aprendizado, no entanto, por outro lado, a visibilidade das mudanças é bem baixa, deixando você sem ter certeza se o comando realmente funcionou na ponta, afinal após a execução do comando, a máquina passa a ter um estado indefinido.

Orquestradores são muito utilizados para a administração de sistemas, eles agregam valor complementando e apoiando as ferramentas de CM. Eles geralmente são utilizadas para fazer deploy de aplicações, manutenções pontuais e emergenciais em datacenters. Também são usados para fazer manutenção e rollout de agentes CM como Puppet e Chef.

Dentro da infra ágil nós segmentamos orquestadores em dois tipos, aqueles que fazem PUSH e os que aqueles que usam MQ.

**Orquestração em arquitetura PUSH**

As ferramentas de orquestração em arquitetura PUSH normalmente utilizam o protocolo SSH para se conectar aos nós e executar ações, dentre elas podemos citar Ansible, Capistrano e Fabric. Elas dependem integralmente de um protocolo externo, não usam agentes locais e no seu desenho a gerência de estados, convergência, abstração, visibilidade plena e idempotência não são parte nativa de suas premissas.

**Orquestração em Arquitetura MQ**

As ferramentas de orquestração em arquitetura MQ utilizam sistemas de mensageria, neste modelo a ferramenta consome uma fila ou tópico que pode ter comandos injetados pelo sysadmin. Uma vez que comandos são injetados na fila, o agente orquestrador recebe, valida e executa em seu node. O Marionette Mcollective é um exemplo de ferramenta deste tipo, ele usa o ActiveMQ como MQ, outro exemplo conhecido é o Salt, ele tem um subsistema de orquestração que usa o projeto ZeroMQ para orquestrar seus Minions.

Estes orquestadores dependem do sistema MQ, são extremamente sensíveis quanto a sincronia de data e hora entre nodes e serviço MQ, não usam agentes, e como nos orquestradores PUSH o seu desenho não acolhe a gerência de estados, convergência, abstração, visibilidade plena e idempotência.

Em infra ágil os orquestadores são ferramentas importantes para apoiar o seu sistema de gerência de configurações e para agregar valor ao processo de automação.

#### 1.2 Provisionamento

Na maioria das organizações o provisionamento é um ponto muito sensível e polêmico.

Existem organizações que levavam até 90 dias para entregar uma simples VM para um time fazer testes. Isto ocorre devido a processos lentos, burocráticos, ineficientes e pelo uso de método artesão para criação e configuração destes ambientes ao final do processo.

Em Infra Ágil a criação de novos ambientes deve ser um processo simples e deve levar minutos ao invés de horas, dias, semanas ou meses.

Para isto são combinados alguns fatores:

- Autoserviço
- Cotas
- Automação

A utilização ferramentas de autoserviço permite que o cliente possa criar seu ambiente diretamente. Quando falamos de um time de TI, criar VMs é algo natural, profissionais de TI estão familiarizados com a criação de VMs ou instâncias na nuvem para testes ou estudos, portanto, os processo de criação de VM no hypervisor da empresa, a criação instâncias na nuvem interna ou a criação de containers devem ser processos naturais e simples.  

Não faz sentido burocratizar uma demanda cotidiana e inserir pessoas entre a necessidade de um time ou de um profissional e o recurso de TI a ser utilizado.

Existem diversas ferramentas de autoserviço que podem ser utilizadas e cada time pode ter suas cotas de uso do hypervisor, nuvem, container e storage. O controle através das quotas garante o uso simplificado e ao mesmo tempo protege, controla e condiciona os times a usarem racionalmente os recursos de TI de sua organização.

Para chegar neste resultado, as ferramentas de automação são integradas ao sistema de autoserviço para criar VMs, instâncias, e containers de forma automatizada. Esta ação converge todos os requisitos em um único processo que fará o bootstrap do sistema operacional, configurações estruturantes de rede, regras de firewall, configuração da sistema operacional, instalação e configuração da app, dentre outras características.

Em Infra Ágil o provisionamento de ambientes deve estar acessível a todos do time de TI de forma inteligente, rápida e segura.

### 2. Entrega

Foco em uma entrega com qualidade, automatizada e rápida.

- Controle de versão
- Repositório de artefatos
- Esteira
- Testes
- Rollback

É importante entender o que não é entrega:

- Receber arquivos anexos via e-mail para publicar em N nodes;
- Editar arquivos no servidor diretamente;
- Publicar algo que não passou por testes mínimos;
- Publicar algo que não tenha plano de rollback.

Todo o código deve estar versionado, isso é obrigatório, seja código da aplicação ou o código infra que vai rodar a app.

Para algo ser publicado, antes deve ocorrer um rigoroso processo de testes, após estes testes - caso a aplicação seja aprovada em todos os testes, o artefato deve ser armazenado em local adequado para permitir a automação da etapa final de publicação e do rollback para uma versão anterior caso necessário - também disponível neste mesmo repositório.

Cabe ao time de sysadmins entender quais são os requisitos para uma aplicação entrar em produção e providenciar a automação desse
processo através de ferramental adequado. O negócio deve entregar premissas claras para publicação, tais como url do repositório, versão, branch, tag, etc.

Não é papel da equipe de infraestrutura autorizar ou decidir se algo deve ser publicado ou não, essa competência não cabe a esse time, quem deve dizer se algo deve ou não ir para produção são os testes e o time de negócio.

Em infra ágil não existe chamados ou tickets de deploy pois o processo deve ser automatizado e o controle dele - seu gatilho, fica com a área de negócio.

Os testes que normalmente cabem ao time de infraestrutura são testes de aceitação do ambiente provisionado, testes do código que vai configurar o SO e a aplicação, testes de carga da aplicação e testes de segurança do ambiente.

O ideal é que toda essa automação em algum momento facilite a integração e a cooperação entre os times Ops e Devs.

Em Infra Ágil devemos fazer a entrega de novas versões de uma aplicação de forma segura, eficiente e com baixo risco.

### 3. Métricas

Não há gerência ou administração de sistemas de infraestrutura sem métricas.

 - Gerar dados
 - Coletar dados
 - Processar dados
 - Armazenar dados
 - Visualizar dados
 - Transformar dado em informação
 - Consumir informações

 Sysadmins normalmente focam muito na monitoração da infraestrutra básica e se esquecem das métricas mais importantes para o negócio de seus clientes.

 Monitorar é importante e essencial, contudo, nem sempre conseguimos monitorar o que precisamos pois certos dados não existem e precisam ser gerados para que possamos entendê-los, estudá-los e tomar ações a partir deles.

 1. Como você sabe se a performance da principal app do seu cliente melhorou ou piorou após a última publicação?

 2. Como você mensura a quantidade real de clientes utilizando a sua aplicação?

 3. Como você mensura a saúde geral de sua aplicação e do seu ambiente?

 Veja que isso vai muito além de checar processo, porta em servidor ou string de retorno de alguma página.

 - Determinadas métricas são vão existir se nós as criarmos;
 - Determinadas informações só poderão ser consumidas se houver métricas para sustentá-las;
 - Métricas precisam ser geradas, coletadas e armazenas em local adequado;

 Existindo tais métricas podemos criar informações realmente úteis sobre nossas aplicações e infraestrutura.

 É muito importante que a aplicação desde a sua concepção já pense em métricas e informações para que seja possível medir sua saúde e informações estruturais, mas se tais dados não existirem, deverão ser criados.

 Métrica claras permitem inclusive a integração do seu monitoramento com sua automação e provisionamento, suas métricas podem ser o gatilho para processos internos de escalabilidade ou redução de recursos no caso de pouca demanda.

 Em infra ágil as métricas são os fundamentos da gestão de sua infraestrutura, elas vão possibilitar planejamento, vão te te permitir analisar cenários e incidentes e vão proporcionar respostas importantes para seus clientes e usuários.

### 4. Pessoas

 Pessoas tem que se entender parte do time e se integrar a ele.

 - Aplicação de métodos ágeis
 - Integração do time
 - Nivelamento e compartilhamento
 - Definição de valores
 - Definição de objetivos

 Depois de falarmos de tantos modelos e tecnologias precisamos falar de pessoas, afinal não é possível ter tantos modelos, tecnologias e continuar funcionando em uma estrutura de equipe clássica.

 Talvez a parte mais sensível de uma iniciativa de infra ágil seja a mudança na forma da equipe se comunicar e trabalhar.
 Neste modelo as pessoas precisam se comunicar, se expressar, precisam ter liberdade criativa, precisam de direcionamento e principalmente de foco.

 Em uma equipe ágil precisamos:

- Incentivar o compartilhamento de conhecimento
- Incentivar a integração de sua equipe
- Ajudar sua equipe a encontrar o que ela precisa para se motivar
- Oferecer os meios para que eles tenham direcionamento e consciência do trabalho
- Incentivar a criatividade e a inovação

Para ajudar seu time, faça um planejamento colaborativo de curto, médio e longo prazo, tentem responder juntos as seguintes perguntas:

- Qual o estado desejado de nossa infra daqui a 3, 6, 9 e 12, 24 meses?
- O que nossa organização está buscando em 24 meses?
- Qual resultado estamos buscando como equipe?

Ajude-os a enxergar o caminho a ser percorrido e tudo será mais fácil.

Lembre-se que se as pessoas estão motivadas, se elas tem liberdade criativa, sem tem incentivo, direcionamento e os recursos necessários para fazer seu trabalho isto vai se refletir em resultados positivos para o profissional, para a equipe e para a organização.

Métodos que podem ser utilizados e adaptados para times de infraestrutura:

- Kanban
- Scrum
- Dojos
- Gamification
- Hacklabs
- Hackatons
- Quaisquer métodos ágeis
- Prática de esportes coletivos
- Práticas coletivas recreativas

 Adapte os métodos a sua organização e ao seu time.

 A integração do seu time deve acontecer dentro e fora do local de trabalho, entretenimento externo é importante.

 Apesar de todos os métodos e tecnologias, o importante são as sempre as pessoas.
