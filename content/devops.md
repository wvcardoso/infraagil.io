+++
date = "2016-06-30T22:59:32-03:00"
menu = "main"
title = "DevOps?"
type = "about"
weight = -220
+++
Tanto DevOps quanto Infra Ágil tem como valores fundamentais o CAMS (Culture, Automation, Measurement and Sharing) e o ICE (Inclusion, Complex Systems and Empathy),

Enquanto DevOps é essencialmente uma cultura, que propõe um processo de transformação para alcançar **toda** organização, fazendo isto através da integração, colaboração e do feedback entre os times,  Infra Ágil propõe os mesmos valores focando na infraestrutura/operação de TI de uma organização.

Uma Infraestrutura de TI tradicional é geralmente  bem heterogênea e contém diversos papéis bem segmentados (Sysadmins, Analistas de rede, DBAs, etc). O que normalmente encontramos são equipes de administração de SO separadas da equipe de banco de dados, backup, rede, virtualização e storage, há também cenários em que temos uma equipe monolítica que interage com diversos fornecedores, as vezes um fornecedor para cada papel da equipe.

Em cenários como esse,  implantar DevOps pode ser um desafio, isso porque há uma grande dificuldade de comunicação entre as equipes de infraestrutura e praticamente não há qualquer comunicação com equipes de desenvolvimento e qualidade.

Melhorar a produtividade de infraestrutura de uma organização é o foco da Infra Ágil através da Automação, Entrega, Métricas e Pessoas. Para isso, usa-se as diversas práticas de Agile (Kanban, Infrastructure as Code, etc.) sem menosprezar coisas relacionadas à SLA, tickets, etc
